﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ConsortioPiperLibrary.Interfaces;

namespace ConsortioPiperLibrary
{
    public class PiperFunctions : IPiperFunctions
    {
        private const string RegexPattern = @"([\w\-\+\.\'\""\ ]+)";

        public string PipeInput(string input, PiperFormat format, bool shortenRanges = false)
        {
            var values = GetValues(input);

            string pipedInput;
            switch (format)
            {
                case PiperFormat.Nav:
                    pipedInput = shortenRanges ? ShortenRanges(values) : string.Join("|", values);
                    break;

                case PiperFormat.SqlServer:
                    values = values.Select(s => $"'{s}'");
                    pipedInput = string.Join(",", values);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(format), format, null);
            }

            return pipedInput;
        }

        public string UnpipeInput(string input, PiperFormat fromFormat)
        {
            string unpipedInput;
            switch (fromFormat)
            {
                case PiperFormat.Nav:
                    unpipedInput = input.Replace("|", Environment.NewLine);
                    break;
                case PiperFormat.SqlServer:
                    var values = GetValues(input);
                    unpipedInput = string.Join(Environment.NewLine, values);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(fromFormat), fromFormat, null);
            }

            return unpipedInput;
        }

        private static IEnumerable<string> GetValues(string input)
        {
            var rx = new Regex(RegexPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            var matches = rx.Matches(input);

            var values = new List<string>();
            values.AddRange(from Match match in matches select match.Value);

            return values;
        }

        private static string ShortenRanges(IEnumerable<string> values)
        {
            List<int> parsedList;
            try
            {
                parsedList = values.Select(int.Parse).Distinct().ToList();
            }
            catch
            {
                return null;
            }

            parsedList.Sort();
            var valuesToPipe = new List<string>();

            var prevInt = int.MinValue;
            var startRangeInt = int.MinValue;
            var endRangeInt = int.MinValue;
            var isBuildingRange = false;
            foreach (var parsedInt in parsedList)
            {
                var isPrevConsecutive = (prevInt + 1 == parsedInt);
                if (isPrevConsecutive)
                {
                    if (!isBuildingRange)
                        startRangeInt = prevInt;

                    endRangeInt = parsedInt;
                    isBuildingRange = true;
                }
                else
                {
                    if (isBuildingRange)
                    {
                        valuesToPipe.Remove(startRangeInt.ToString());
                        valuesToPipe.Add($"{startRangeInt}..{endRangeInt}");
                    }

                    valuesToPipe.Add(parsedInt.ToString());
                    isBuildingRange = false;
                }

                prevInt = parsedInt;
                if (parsedInt != parsedList.Last())
                    continue;

                if (isBuildingRange)
                {
                    valuesToPipe.Remove(startRangeInt.ToString());
                    valuesToPipe.Add($"{startRangeInt}..{endRangeInt}");
                }
            }

            return string.Join("|", valuesToPipe);
        }
    }
}