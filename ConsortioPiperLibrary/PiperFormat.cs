﻿using System.ComponentModel;
using ConsortioPiperLibrary.Extensions;

namespace ConsortioPiperLibrary
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum PiperFormat
    {
        [Description("Dynamics NAV")]
        Nav,
        [Description("SQL Server")]
        SqlServer
    }
}