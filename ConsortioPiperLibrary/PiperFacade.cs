﻿using System;
using ConsortioPiperLibrary.Interfaces;

namespace ConsortioPiperLibrary
{
    public static class PiperFacade
    {
        private static readonly IPiperFunctions PiperFunctions = new PiperFunctions();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <param name="shortenRanges"></param>
        /// <returns></returns>
        public static bool PipeInput(string input, out string output, bool shortenRanges = false)
        {
            output = PiperFunctions.PipeInput(input, PiperFormat.Nav, shortenRanges);

            return !string.IsNullOrEmpty(output);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="format"></param>
        /// <param name="output"></param>
        /// <param name="shortenRanges"></param>
        /// <returns></returns>
        public static bool PipeInput(string input, PiperFormat format, out string output, bool shortenRanges = false)
        {
            output = PiperFunctions.PipeInput(input, format, shortenRanges);

            return !string.IsNullOrEmpty(output);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="fromFormat"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        public static bool UnpipeInput(string input, PiperFormat fromFormat, out string output)
        {
            output = PiperFunctions.UnpipeInput(input, fromFormat);

            return !string.IsNullOrEmpty(output);
        }
    }
}