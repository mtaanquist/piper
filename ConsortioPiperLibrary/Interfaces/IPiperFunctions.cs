﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsortioPiperLibrary.Interfaces
{
    public interface IPiperFunctions
    {
        string PipeInput(string input, PiperFormat format, bool shortenRanges = false);
        string UnpipeInput(string input, PiperFormat fromFormat);
    }
}
