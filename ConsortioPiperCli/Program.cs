﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsortioPiperLibrary;

namespace ConsortioPiperCli
{
    class Program
    {
        static void Main(string[] args)
        {
            string output;
            PiperFacade.PipeInput("test1,test2,test3,", out output);

            Console.WriteLine(output);
            Console.ReadKey();
        }
    }
}
