﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConsortioPiperLibrary.Tests
{
    [TestClass]
    public class PiperFacadeTests
    {
        [TestMethod]
        public void PipeInputTest()
        {
            const string expected = "test1|test2|test3";

            string[] delimiters = {'\t'.ToString(), Environment.NewLine, ",", ";"};
            foreach (var delimiter in delimiters)
            {
                var input = string.Format("test1{0}test2{0}test3{0}", delimiter);

                string actual;
                var result = PiperFacade.PipeInput(input, out actual);

                Assert.AreEqual(expected, actual);
                Assert.IsTrue(result);
                Debug.WriteLine($"Input => {input}\nResult => {actual}");
            }
        }

        [TestMethod]
        public void PipeInputWithSpecialCharactersTest()
        {
            const string expected =
                "DSCFRÜHFL07|GLAS-INLAY-GRÜN|X-K9228-BOD-O.+TÜR|X-K9228-BOD-O.+TÜR-L|X-N-DÜBEL-4ST-8X30|X-N-DÜBEL-4ST-8X30-L";
            const string input =
                "DSCFRÜHFL07\nGLAS-INLAY-GRÜN\nX-K9228-BOD-O.+TÜR\nX-K9228-BOD-O.+TÜR-L\nX-N-DÜBEL-4ST-8X30\nX-N-DÜBEL-4ST-8X30-L";

            string actual;
            var result = PiperFacade.PipeInput(input, out actual);

            Assert.AreEqual(expected, actual);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void PipeInputWithQuotationMarksTest()
        {
            const string expected =
                "DSCFRÜH\"FL07|GLAS-INLAY-GRÜN|X-K9228-BOD\'-O.+TÜR|X-K9228-BOD-O.+TÜR-L|X-N-DÜBEL-4ST-8X30|X-N-DÜBEL-4ST-8X30-L";
            const string input =
                "DSCFRÜH\"FL07\nGLAS-INLAY-GRÜN\nX-K9228-BOD\'-O.+TÜR\nX-K9228-BOD-O.+TÜR-L\nX-N-DÜBEL-4ST-8X30\nX-N-DÜBEL-4ST-8X30-L";

            string actual;
            var result = PiperFacade.PipeInput(input, out actual);

            Assert.AreEqual(expected, actual);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ShortenRangesTest()
        {
            const string expected = "900|1000..1005|1100|1200..1202|1300";
            const string input = "900,1000,1001,1002,1003,1004,1100,1200,1005,1201,1202,1202,1300";

            string actual;
            PiperFacade.PipeInput(input, PiperFormat.Nav, out actual, shortenRanges: true);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void UnpipeInputToNewLinesTest()
        {
            var expected = $"test1{Environment.NewLine}test2{Environment.NewLine}test3";
            var input = "test1|test2|test3";

            string actual;
            var result = PiperFacade.UnpipeInput(input, PiperFormat.Nav, out actual);

            Assert.AreEqual(expected, actual);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void UnpipeInputWithSpecialCharsToNewLinesTest()
        {
            var expected = $"X-K9228-BOD-O.+TÜR{Environment.NewLine}X-K9228-BOD-O.+TÜR-L{Environment.NewLine}X-N-DÜBEL-4ST-8X30";
            var input = "X-K9228-BOD-O.+TÜR|X-K9228-BOD-O.+TÜR-L|X-N-DÜBEL-4ST-8X30";

            string actual;
            var result = PiperFacade.UnpipeInput(input, PiperFormat.Nav, out actual);

            Assert.AreEqual(expected, actual);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ShouldNotPipeSingleSpace()
        {
            var expected = $"ORDRE|ORDRE LAG";
            var input = $"ORDRE{Environment.NewLine}ORDRE LAG{Environment.NewLine}";

            string actual;
            var result = PiperFacade.PipeInput(input, PiperFormat.Nav, out actual);

            Assert.AreEqual(expected, actual);
            Assert.IsTrue(result);
        }
    }
}