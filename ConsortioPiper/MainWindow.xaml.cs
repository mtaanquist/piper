﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using ConsortioPiperLibrary;

namespace ConsortioPiper
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        // Commands
        public static RoutedCommand PipeCommand = new RoutedCommand();
        public static RoutedCommand UnpipeCommand = new RoutedCommand();
        public static RoutedCommand ClearCommand = new RoutedCommand();

        // Properties
        private PiperFormat _selectedPiperFormat = PiperFormat.Nav;
        public PiperFormat SelectedPiperFormat
        {
            get { return _selectedPiperFormat; }
            set
            {
                _selectedPiperFormat = value;
                OnPropertyChanged();
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            // Add gestures
            PipeCommand.InputGestures.Add(new KeyGesture(Key.P, ModifierKeys.Alt));
            UnpipeCommand.InputGestures.Add(new KeyGesture(Key.U, ModifierKeys.Alt));
        }

        private void PipeCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            PiperFormat format;
            var enumParseResult = Enum.TryParse(FormatComboBox.Text, out format);
            if (!enumParseResult)
                format = PiperFormat.Nav;

            var shortenRanges = (ShortenRangesCheckBox.IsChecked.HasValue && ShortenRangesCheckBox.IsChecked.Value);

            string outputText;
            PiperFacade.PipeInput(InputTextBox.Text, format, out outputText, shortenRanges);
            OutputTextBox.Text = outputText;

            if (ClipboardCheckBox.IsChecked.HasValue && ClipboardCheckBox.IsChecked.Value &&
                !string.IsNullOrEmpty(outputText))
            {
                Clipboard.SetText(outputText);
            }
        }

        private void UnpipeCommandExecuted(object sender, RoutedEventArgs e)
        {
            PiperFormat format;
            var enumParseResult = Enum.TryParse(FormatComboBox.Text, out format);
            if (!enumParseResult)
                format = PiperFormat.Nav;

            string outputText;
            PiperFacade.UnpipeInput(OutputTextBox.Text, format, out outputText);
            InputTextBox.Text = outputText;

            if (ClipboardCheckBox.IsChecked.HasValue && ClipboardCheckBox.IsChecked.Value &&
                !string.IsNullOrEmpty(outputText))
            {
                Clipboard.SetText(outputText);
            }
        }

        private void ClearCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            InputTextBox.Text = string.Empty;
            OutputTextBox.Text = string.Empty;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}